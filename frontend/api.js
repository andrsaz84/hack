import Vue from 'vue'

const api = process.browser ? require('parse') : require('parse/node')
const bus = new Vue()

api.initialize('myAppId', 'client')
api.serverURL = 'http://localhost:1337/parse'

export { api, bus }
