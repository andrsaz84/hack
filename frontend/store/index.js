import { api, bus } from '~/api.js'

export const state = () => ({
  user: 0,
  circles: [],
  categories: []
})

export const mutations = {
  switchCurrent (state) {
    state.user = (state.user + 1) % 4
  },
  setCircles (state, circles) {
    state.circles = circles
  },
  setCategories (state, categories) {
    state.categories = categories
  }
}

export const actions = {
  async makeCircles ({ commit }) {
    await api.User.logIn('user', 'user')
    bus.$emit('logged in')

    const query = new api.Query(api.Object.extend('Point'))
    const points = await query.find({})

    let circles = []
    for (let key in points) {
      let point = points[key]
      circles.push({
        id: point.id,
        lat: point.get('center').latitude,
        lng: point.get('center').longitude,
        name: point.get('name'),
        radius: point.get('radius'),
        userId: point.get('userId'),
        categoryId: point.get('categoryId')
      })
    }

    commit('setCircles', circles)
  },
  async nuxtServerInit ({ commit, dispatch }) {
    await dispatch('makeCircles')

    const search = new api.Query(api.Object.extend('Category'))
    const fetched = await search.find()

    let categories = []
    for (let key in fetched) {
      let category = fetched[key]
      categories.push({
        id: category.id,
        name: category.get('name')
      })
    }

    commit('setCategories', categories)
  },
  switchUser ({ commit }) {
    commit('switchCurrent')
    bus.$emit('logged in')
  }
}
